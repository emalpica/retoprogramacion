import React, { Component } from 'react';
import ListItemBanner from './../tsx/ListItemBanner';

class Banner extends Component{
	render(){
		return(
			<>
			    <div className="ProductBanner__background">
                </div>
                <h1 className="Typografy__principal">Seguro de
                    <br />  
                    <strong>Salud</strong>
                </h1>
                <ListItemBanner/>
                <span  className="ProductBanner__footer">2020 RIMAC Seguros y Reaseguro</span>
			</>
		)
	}
}

export default Banner