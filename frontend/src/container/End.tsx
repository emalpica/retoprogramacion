import React from 'react'
import '../components/Button.scss'

const Button = () => {
    return(
        <div className="Register Register__confirmation">
            <h3 className="Typografy__secundary">¡Gracias por  <strong className="Color__primary">confiar en nosotros!</strong></h3>
            <p className="margin-sm">Queremos conocer mejor la salud ðe los asegurados. Un asesor <strong> se pondrá en contacto</strong>
                contigo en las siguientes <strong>48 horas.</strong></p>
            <div className="Button__container">
                <button className="Button Button__primary"><strong>IR A SALUD</strong></button>
            </div>
        </div>
    )
}

export default Button