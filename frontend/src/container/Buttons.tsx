import React, { Component } from 'react'
import { ReactNode, FunctionComponent } from 'react';
import { useHistory } from "react-router-dom";
import '../components/Layout.scss'
import '../styles/general.scss'

type Props = {

};


const Buttons = () => {
	let history = useHistory();
	const HandleSubmit = () => {
		history.push('/RegisterStepEnd')
	}
	return (
		<div className="CardBenefits__accordeon-buttons">
			<button className="Button Button__terciary" onClick={HandleSubmit}>ENVIAR COTIZACION POR CORREO</button>
			<button className="Button Button__primary" onClick={HandleSubmit}>COMPRAR PLAN</button>
		</div>
	)

}
export default Buttons