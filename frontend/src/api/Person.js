const baseUrl = 'https://freestyle.getsandbox.com/dummy'
const apiKey = ''

export async function getDataPerson () {
    const response = await fetch(`${baseUrl}/obtenerdatospersona`)
    const responseJson = await response.json()
    return responseJson
  }
  
  export default {
    getDataPerson,
  }
