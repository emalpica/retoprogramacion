import React from 'react';
import Home from './pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/styles/general.scss';


function App() {


  return (
    <div>
        <main>
           <Home
           />
        </main>
    </div>
  );
}

export default App;
