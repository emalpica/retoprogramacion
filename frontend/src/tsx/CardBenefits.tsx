import React, { Component } from 'react'
import {ReactNode, FunctionComponent } from 'react';
import '../components/CardBenefit.scss';
import '../styles/general.scss';
import '../components/Button.scss';


type Props = {
    nombreCobertura? : string | '' |null,
    monto? : string | '' |null,
    plan? : string | '' |null,
};


export class CardBenefits extends Component<Props> {
    
     render(){
        const {nombreCobertura,  monto, plan } = this.props
         return(
            <div className="CardBenefits__content">
                <div className="CardBenefits__principal">
                    <h5>{nombreCobertura}</h5>
                    <h2 className="CardBenefits__mount">{monto}</h2>
                    <button className="Button Button__secundary">
                        {plan}
                    </button>
                </div>
                <div className="CardBenefits__secundary">
                    <img className="CardBenefits__img"/>
                </div>
            </div>
         )
      }
}

export default CardBenefits
