import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Form, Col, Row } from 'react-bootstrap';
import '../components/Button.scss';
import '../components/floating-labels.scss';
import '../components/Input.scss';
import '../components/Register.scss';
import '../components/Form.scss';
import '../components/Step.scss';

interface Props {
  persona: object | any | null;
}

const RegisterFormTwo:React.FC<Props> = ({ persona }) => {
  const [validated, setValidated] = useState(false);
  const [dni, setDni] = useState(persona.dni);
  const [nacimiento, setNacimiento] = useState(persona.nacimiento);
  const [nombres, setNombres] = useState('');
  const [materno, setMaterno] = useState('');
  const [paterno, setPaterno] = useState('');
  const [sexo, setSexo] = useState(false);
  const [asegurar, setAsegurar] = useState(false);

  let history = useHistory();
  const handleSubmit = (event: any) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {

      event.preventDefault();
      event.stopPropagation();
    }
    else{
      persona.dni= dni;
      persona.nacimiento= nacimiento;
      persona.nombres= nombres;
      persona.materno= materno;
      persona.paterno= paterno;

      history.push('/RegisterStepTwo')
    }
    setValidated(true);

  };

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Form.Row>
        <Form.Group as={Col} controlId="validationCustom01" className="form-label-group">
            <Form.Control
              required
              type="text"
              placeholder="Nro Documento"
              onChange={ (dni) => setDni( dni.target.value) }
              defaultValue = {dni}
            />
            <Form.Label>Nro Documento</Form.Label>
            <Form.Control.Feedback>Correcto</Form.Control.Feedback>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        <Form.Group as={Col} md="12" className="form-label-group" controlId="validationCustom02">
          <Form.Control
            required
            type="text"
            placeholder="Nombres"
            onChange={ (nombres) => setNombres( nombres.target.value) }
            defaultValue = {nombres}
          />
          <Form.Label>Nombres</Form.Label>
          <Form.Control.Feedback>Correcto</Form.Control.Feedback>
        </Form.Group>
      </Form.Row> 
      <Form.Row> 
        <Form.Group as={Col} md="12" className="form-label-group" controlId="validationCustom03">
          <Form.Control
            required
            type="text"
            placeholder="Apellido Materno"
            onChange={ (materno) => setMaterno( materno.target.value) }
            defaultValue = {materno}
          />
          <Form.Label>Apellido Materno</Form.Label>
          <Form.Control.Feedback>Correcto</Form.Control.Feedback>
        </Form.Group>
      </Form.Row>
      <Form.Row> 
        <Form.Group as={Col} md="12" className="form-label-group" controlId="validationCustom04">
          <Form.Control
            required
            type="text"
            placeholder="Apellido Paterno"
            onChange={ (paterno) => setPaterno( paterno.target.value) }
            defaultValue = {paterno}
          />
          <Form.Label>Apellido Paterno</Form.Label>
          <Form.Control.Feedback>Correcto</Form.Control.Feedback>
        </Form.Group>
      </Form.Row>
      <Form.Row> 
        <Form.Group as={Col} md="12" className="form-label-group" controlId="validationCustom05">
          <Form.Control
            required
            type="date"
            placeholder="Fecha de Nacimiento"
            onChange={ (nacimiento) => setNacimiento( (nacimiento.target.value).toString()) }
          />
          <Form.Label>Fecha de Nacimiento</Form.Label>
          <Form.Control.Feedback>Correcto</Form.Control.Feedback>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        <div className="Register__form-select">
          <p>Género</p>
          <Form.Group as={Row}>
          <Col sm={12}>
            <Form.Check
              className="form-check Form__radio"
              type="radio"
              label="Masculino"
              name="formHorizontalRadios1"
              id="formHorizontalRadios1"
              checked={true}
            />
            <Form.Check
              className="form-check Form__radio"
              type="radio"
              label="Femenino"
              name="formHorizontalRadios1"
              id="formHorizontalRadios8"
            />
        </Col>
        </Form.Group>
        </div>
      </Form.Row>
      <Form.Row>
        <div className="Register__form-select">
          <p>¿A quién vamos a asegurar?</p>
          <Form.Group as={Row}>
            <Col sm={12}>
              <Form.Check
                className="form-check Form__radio"
                type="radio"
                label="Solo a mí"
                name="formHorizontalRadios"
                id="formHorizontalRadios3"
                feedbackTooltip
              />
              <Form.Check
                className="form-check Form__radio"
                type="radio"
                label="A mí y a mi familia"
                name="formHorizontalRadios"
                id="formHorizontalRadios6"
                checked={true}
              />
          </Col>
          </Form.Group>
        </div>
      </Form.Row>
      <div className="Button__container mt-5">
        {
          dni!='' && nombres!='' && materno!='' && paterno!='' && nacimiento!=''
          ?
            <button type="submit" className="Button Button__primary">COMENCEMOS</button>
          :
            <button type="submit" className="Button Button__primary Button__disable">COMENCEMOS</button>
        }
      </div>
    </Form>

    );
};

export default RegisterFormTwo;