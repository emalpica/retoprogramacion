import React, { Component } from 'react'
import {ReactNode } from 'react';
import '../components/Layout.scss'
import '../components/ProductBanner.scss'
import '../components/Register.scss'

type Props = {
      classRegister?: string | '' |null,
      children: ReactNode,
};


export class LayoutContent extends Component<Props> {
     render(){
        const {classRegister } = this.props
         return(
            <div className="Layout__content">
              <div className= {`Register ${classRegister ? classRegister : ''}`}>
                { this.props.children }
              </div>
            </div>
         )
      }
}

export default LayoutContent
