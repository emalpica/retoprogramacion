import React, { Component } from 'react'
import '../components/Layout.scss'
import '../styles/general.scss'
import '../components/CardBenefit.scss';
import '../components/Ico.scss'


type Props = {
    classItem? : string | '' |null,
    ciudad? : string | '' |null,
    descripcion? : string | '' |null,
};


export class CardBenefitsItems extends Component<Props> {
    
     render(){
        const {classItem,  ciudad, descripcion } = this.props
         return(
            <li className= {`CardBenefits__item ${classItem ? classItem : ''}`}>
				<img className="Ico__heart" />{ciudad} <span>{descripcion}</span>
			</li>
         )
      }
}

export default CardBenefitsItems
