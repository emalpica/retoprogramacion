import React, { Component } from 'react'
import {ReactNode, FunctionComponent } from 'react';
import '../components/Layout.scss'
import '../styles/general.scss'

type Props = {
      classLayout?: string | '' |null,
      children: ReactNode,
};


export class Layout extends Component<Props> {
    
     render(){
        const {classLayout, children } = this.props
         return(
             <div className= {`Layout__wrapper ${classLayout ? classLayout : ''}`}>
                 { this.props.children }
             </div>
         )
      }
}

export default Layout
