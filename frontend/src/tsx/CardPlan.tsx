import React, {  useState, SyntheticEvent } from 'react'
import '../components/Layout.scss'
import '../styles/general.scss'

interface Props  {
    classActive?: string | '' |null,
    nombrePlan? : string | '' |null,
    monto? : string | '' |null,
    time? : string | '' |null,
    person: object | any | null,
    index?: string | number |'' |null,
    getPlan: (e: SyntheticEvent) => void;
};

const CardPlan:React.FC<Props> = ({ person, time, monto, nombrePlan, index, classActive, getPlan }) => {
    
    const [active, setActive] = useState(false); 
    const Plan = {
        name: nombrePlan,
        price: monto,
        period: time,
        index:index
    }
    const getCurrentPlan = () =>{
        person.plan = Plan;
        getPlan(person);
    }

         return(
             <a onClick={getCurrentPlan}>
                <div className= {`CardPlan__item  ${classActive ? classActive : ''}`}>
                    <div className="CardPlan__img">
                        <img className="CardPlan__check" />
                    </div>
                    <span className="CardPlan__title">{nombrePlan}</span>
                    <span>S/.<span className="CardPlan__mount">{monto}</span></span>
                    <span className="CardPlan__item-time">{time}</span>
                </div>
             </a>
         )
}
export default CardPlan
