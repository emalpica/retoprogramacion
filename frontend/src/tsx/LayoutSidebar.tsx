import React, { Component } from 'react'
import {ReactNode } from 'react';
import '../components/Layout.scss'
import '../components/ProductBanner.scss'

type Props = {
      classLayoutSidebar?: string | '' |null,
      classLayoutBackground?: string | '' |null,
      classLayoutSidebarContent?: string | '' |null,  
      children: ReactNode,
};


export class LayoutSidebar extends Component<Props> {
     render(){
        const {classLayoutSidebar, classLayoutBackground, classLayoutSidebarContent } = this.props
         return(
            <div className = {`Layout__sidebar ProductBanner ${classLayoutSidebar ? classLayoutSidebar : ''}`}>
                <div className = {`ProductBanner__content ${classLayoutSidebarContent ? classLayoutSidebarContent : ''}`}>
                    <div className= {`ProductBanner__background ${classLayoutBackground ? classLayoutBackground : ''}`} ></div>
                        { this.props.children }
                </div>
            </div>
         )
      }
}

export default LayoutSidebar
