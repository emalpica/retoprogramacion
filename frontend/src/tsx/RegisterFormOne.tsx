
import React, { SyntheticEvent, useState } from 'react';
import { useHistory } from "react-router-dom";
import { Form, Col } from 'react-bootstrap';
import '../components/Button.scss';
import '../components/floating-labels.scss';
import '../components/Input.scss';
import '../components/Register.scss';
import '../components/Form.scss';

interface Props {
  person: object | any | null;
  nextStep: (e: SyntheticEvent) => void;

}

const RegisterFormOne:React.FC<Props> = ({ person, nextStep }) => {

  const [validated, setValidated] = useState(false);
  const [dni, setDni] = useState('');
  const [fecha, setFecha] = useState('');
  const [movil, setMovil] = useState('');

  let history = useHistory();
  const handleSubmit = (event: any) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    else{
      person.dni= dni;
      person.fecha= fecha;
      person.movil= movil;
      nextStep(person);
      
      history.push('/RegisterStepOne')
    }
    setValidated(true);

  };

  return (
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Row>
          <Form.Group as={Col} controlId="validationCustom01" className="form-label-group">
            <Form.Control
              required
              type="text"
              placeholder="Nro Documento"
              onChange={ (dni) => setDni( dni.target.value) }
              defaultValue = {dni}
            />
            <Form.Label>Nro Documento</Form.Label>
            <Form.Control.Feedback>Correcto</Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} md="12" controlId="validationCustom02" className="form-label-group">
            <Form.Control
              required
              type="date"
              placeholder="Fecha de nacimiento"
              onChange={ (fecha) => setFecha( (fecha.target.value).toString()) }
              defaultValue = {fecha}
            />
            <Form.Label>Fecha de nacimiento</Form.Label>
            <Form.Control.Feedback>Correcto</Form.Control.Feedback>
          </Form.Group>
        </Form.Row> 
        <Form.Row> 
          <Form.Group as={Col} md="12" controlId="validationCustom03" className="form-label-group">
            <Form.Control
                required
                type="text"
                placeholder="Celular"
                onChange={ (movil) => setMovil( movil.target.value) }
                defaultValue = {movil}
            />
            <Form.Label>Celular</Form.Label>
            <Form.Control.Feedback>Correcto</Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <div className="Register__politics">
          <Form.Group className="checkbox Register__politics-item">
            <Form.Check
              required
              label="Acepto la Política de Protección de Datos Personales y los Términos 
              y condiciones"
              feedback="Los terminos y condiciones son obligatorio."
            />
          </Form.Group>
          <Form.Group className="checkbox Register__politics-item">
            <Form.Check
              required
              label="Acepto la Política de Envío de Comunicaciones Comerciales"
              feedback="Las políticas son obligatorio."
            />
          </Form.Group>
        </div>

        <button type="submit" className="Button Button__primary">COMENCEMOS</button>
      </Form>
    );
};

export default RegisterFormOne;