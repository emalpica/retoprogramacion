import React, { Component } from 'react'
import {ReactNode, FunctionComponent } from 'react';
import '../components/Layout.scss'
import '../styles/general.scss'


type Props = {
    children: ReactNode,
};


export class LayoutBlock extends Component<Props> {
  
   render(){
      const { children } = this.props
       return(
            <div className="Layout__block">       
                { this.props.children }
           </div>
       )
    }
}

export default LayoutBlock
