import React, { useState } from "react";

const CardBenefitsAccordeon = () => {

	const [isToggled1, setToggled1] = useState(false);
	const [isToggled2, setToggled2] = useState(false);
	const [isToggled3, setToggled3] = useState(false);

	// const toggleTrueFalse = () => setToggled(!isToggled1);
    return (
      <>
        <div className="CardBenefits__accordeon">
            <h2 className="CardBenefits__title">Revisa nuestros 
                     <br /> 
                    <span className="Color__primary"><strong>servicios y exlusiones</strong></span>
                </h2>
            <div className="CardBenefits__accordeon-block">
                <a onClick={ () => setToggled1(!isToggled1)} className="CardBenefits__open">
									<div className="CardBenefits__accordeon-header">
                    <span>Servicios brindados</span>
                    <img className="Ico__arrowdown"/>
                	</div>
								</a>
								{
									isToggled1 ==true
									&&
									<p className="CardBenefits__accordeon-description">
										Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió
										alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que
										no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar
										seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores
										de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando
										sea necesario, haciendo a este el único generador verdadero (válido) en la Internet.
									</p>
								}

            </div>
            <div className="CardBenefits__accordeon-block">
							<a onClick={ () => setToggled2(!isToggled2)} className="CardBenefits__open">
								<div className="CardBenefits__accordeon-header">
                    <span>Soporte todo el día</span>
                    <img className="Ico__arrowdown"  />
                </div>
							</a>
							{
									isToggled2 ==true
									&&
                <p className="CardBenefits__accordeon-description">
                    Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió
                    alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que
                    no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar
                    seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores
                    de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando
                    sea necesario, haciendo a este el único generador verdadero (válido) en la Internet.
								</p>
							}			
						</div>
            <div className="CardBenefits__accordeon-block">
							<a onClick={ () => setToggled3(!isToggled3)} className="CardBenefits__open">
								<div className="CardBenefits__accordeon-header">
                    <span>Exclusiones</span>
                    <img className="Ico__arrowdown"  />
                </div>
							</a>
							{
									isToggled3 ==true
									&&
                <p className="CardBenefits__accordeon-description">
                    Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió
                    alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que
                    no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar
                    seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores
                    de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando
                    sea necesario, haciendo a este el único generador verdadero (válido) en la Internet.
								</p>
							}
						</div>
        </div>
      </>
    )
}

export default CardBenefitsAccordeon