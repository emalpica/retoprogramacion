import React from 'react'
import '../components/ProductBanner.scss'

const ListItemBanner = () => {
    return(
      <ul className="ProductBanner__list Typografy__terciary">
         <li className="ProductBanner__list-item"><img className="Ico Ico__shield"/>Compralo de manera đacila y rapida</li>
         <li className="ProductBanner__list-item"><img className="Ico Ico__shield"/>Compralo de manera đacila y rapida</li>
         <li className="ProductBanner__list-item"><img className="Ico Ico__shield"/>Compralo de manera đacila y rapida</li>
         <li className="ProductBanner__list-item"><img className="Ico Ico__shield"/>Compralo de manera đacila y rapida</li>
      </ul>
    );
}

export default ListItemBanner;