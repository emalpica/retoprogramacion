import React, { Component } from 'react';
import './../components/Layout.scss';
import './../components/ProductBanner.scss';
import './../components/Typografy.scss';
import './../components/Ico.scss';
import './../components/Register.scss';
import Layout from './../tsx/Layout';
import LayoutSidebare from './../tsx/LayoutSidebar';
import LayoutContent from './../tsx/LayoutContent';
import Banner from './../container/Banner';
import RegisterFormOne from './../tsx/RegisterFormOne';
import RegisterFormTwo from './../tsx/RegisterFormTwo';
import LayoutBlock from './../tsx/LayoutBlock';
import CardPlan from '../tsx/CardPlan';
import CardBenefits from '../tsx/CardBenefits';
import CardBenefitsItems from '../tsx/CardBenefitsItems';
import CardBenefitsAccordeon from '../tsx/CardBenefitsAccordeon';
import End from '../container/End';
import Buttons from '../container/Buttons';
import { getDataPerson } from '../api/Person';
import '../components/CardPlan.scss';
import '../components/Register.scss';
import '../components/CardBenefit.scss';
import '../components/Step.scss';
import '../styles/general.scss';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const PlanSeguro = [
	{ name: 'BÁSICO', price: '160',	period: 'mensual'},
	{ name: 'AVANZADO', price: '200',	period: 'mensual'},
	{ name: 'PREMIUN', price: '250',	period: 'mensual'},
	{ name: 'FULL', price: '500',	period: 'mensual'},
]

export class Home extends Component {
  state = {
	 	dni: '',
	 	nacimiento: '',
	 	politics: false,
	 	term: false,
		 isFetch:false,
		 nombres:'',
	 	paterno: '',
	 	materno: '',
	 	genero: '',
	 	plan: {
			 name: '',
			 price: '',
			 period: '',
			 index: 99,
		 }
	 }
	 
	async componentDidMount () {
		const response = fetch(`https://freestyle.getsandbox.com/dummy/obtenerdatospersona`, {mode: 'no-cors'})
		.then(function(response) {
			console.log(response); 
		}).catch(function(error) {  
			console.log('Request failed', error)  
		});
	}
	
	nextStep = (person: object) => {
	};

	getPlan = (person: any) => {
		 console.log('person',person);

		  this.setState({
				dni: person.dni,
				nacimiento: person.fecha,
				politics: person.politics,
				term: person.term,
				nombres: person.nombres,
				paterno: person.paterno,
				materno: person.materno,
				genero: person.genero,
				plan: person.plan
      })
	}


	render(){

		 const { plan } = this.state;

				return(
					<Router>
						<Switch>
							<Route  path='/RegisterStepEnd'>
								<Layout
									classLayout='Container Layout__wrapper--outer'
								>
									<LayoutSidebare>

									</LayoutSidebare>
									<LayoutContent
										classRegister = "Register__step-end"
									>
										<End/>
									</LayoutContent>
								</Layout>
							</Route>
							<Route  path='/RegisterStepTwo'>
								<Layout
									classLayout='Container Layout__wrapper--outer'
								>
										<LayoutSidebare
											classLayoutSidebar= 'ProductBanner--outer'
											classLayoutBackground= 'ProductBanner__background--outer'
										>
										</LayoutSidebare>
										<LayoutContent
											classRegister = "Register__step-2"
										>
											<div className="Step Color__primary">
												<Link to="/RegisterStepOne"><span className="Step__button"></span></Link>
												<span> PASO 2 <span className="Color__secundary">  DE 7</span></span>
											</div>	
											<LayoutBlock>
												<h3 className="Typografy__secundary">Elige <strong className="Color__primary">tu protección</strong></h3>
												<p className="margin-sm">Selecciona tu plan ideal.</p>
												<div className="CardPlan">
													{
														PlanSeguro.map((card, index) => 
														 <>
															{
																index==plan.index
																?
																	<CardPlan
																		classActive='active'
																		nombrePlan= {card.name}
																		monto= {card.price}
																		time= {card.period}
																		person= {this.state}
																		getPlan= {this.getPlan}
																		index= {index}
																	/>
																:
																	<CardPlan
																		classActive=''
																		nombrePlan= {card.name}
																		monto= {card.price}
																		time= {card.period}
																		person= {this.state}
																		getPlan= {this.getPlan}
																		index= {index}
																	/>
															}
														 </>
														)
													}
												</div>
												<div className="CardBenefits">
													<div className="CardBenefits__header">Cuentas con estos beneficios:</div>
														<CardBenefits
															nombreCobertura= 'Cobertura máxima'
															plan= {plan.name ? plan.name : 'ELEGIR PLAN'}
															monto= {plan.price ? `S/${2 * plan.index + 1}MM` : 'S/.0MM'}//'S/1MM'
														/>
												
													<ul className="CardBenefits__items">
													<CardBenefitsItems
														classItem= ''
														ciudad= 'Lima'
														descripcion='(zona de cobertura)'
													/>
													<CardBenefitsItems
														classItem= 'disable'
														ciudad= 'Lima'
														descripcion='(zona de cobertura)'
													/>
													<CardBenefitsItems
														classItem= 'disable'
														ciudad= '+30 clínicas'
														descripcion='(red afiliada)'
													/>
													<CardBenefitsItems
														classItem= 'disable'
														ciudad= 'Lima'
														descripcion='(zona de cobertura)'
													/>
													<CardBenefitsItems
														classItem= 'disable'
														ciudad= '+30 clínicas'
														descripcion='(red afiliada)'
													/>
												</ul>
												</div>
											
											<CardBenefitsAccordeon/>
											<Buttons/>
											</LayoutBlock>
										</LayoutContent>
								</Layout>
							</Route>			
							<Route  path='/RegisterStepOne'>
								<Layout
									classLayout='Container Layout__wrapper--outer'
								>
									<LayoutSidebare
										classLayoutSidebar= 'ProductBanner--outer'
										classLayoutBackground= 'ProductBanner__background--outer'
									>
									</LayoutSidebare>
									<LayoutContent
										classRegister = "Register__step-1"
									>
										<div className="Step Color__primary">
											<Link to="/"><span className="Step__button"></span></Link>
											<span>PASO 1	<span className="Color__secundary">  DE 7</span></span>
										</div>	
										<LayoutBlock>
											<h3 className="Typografy__secundary mb-3">Hola <strong className="Color__primary">Pepito</strong></h3>
											<p className="margin-sm">Valida que todos los datos sean correctos.</p>
											<h3 className="Typografy__terciary mb-4"> Datos personales del titular</h3>
											<RegisterFormTwo
												persona={this.state}
											/>
										</LayoutBlock>
										</LayoutContent>    
								</Layout>
							</Route>
							<Route path='/' exact>
								<Layout>
									<LayoutSidebare>
										<Banner/>
									</LayoutSidebare>
									<LayoutContent
										classRegister = "Register__step-0"
									>
										<h2 className="Typografy__secundary">Obten tu <strong className="Color__primary">seguro ahora</strong></h2>
										<h3 className="Typografy__terciary">Ingresa los datos para comenzar</h3>
										<RegisterFormOne
											person={this.state}
											nextStep= {this.nextStep}
										/>
									</LayoutContent>    
								</Layout>
							</Route>
							<Route path='/Panel'>

							</Route>				
						</Switch>
						
					</Router>	
				);
		}

}

export default Home